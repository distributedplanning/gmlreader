# README #

### Simple library to read and write graphs ###

This library provides utilities to read/write graphs in [GML](https://en.wikipedia.org/wiki/Graph_Modelling_Language) and [LGF](http://lemon.cs.elte.hu/pub/doc/1.2.3/a00002.html) formats.
Moreover, it can convert both formats into a [graph_msg](https://github.com/davetcoleman/graph_msgs) and thus be used by ROS software.

### How do I get set up? ###

Different wrappers are available as hpp files, they all use a common graph definition found in graph.cpp. See the tests inside src folder for some example.